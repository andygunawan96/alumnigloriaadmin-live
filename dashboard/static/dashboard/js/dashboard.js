function getToken(){
    var csrftoken = getCookie('csrftoken');

    $.ajaxSetup({
       beforeSend: function(xhr, settings) {
           if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
               xhr.setRequestHeader("X-CSRFToken", csrftoken);
           }
       }
    });
}

function getCookie(name)
{
   var cookieValue = null;
   if (document.cookie && document.cookie !== '') {
       var cookies = document.cookie.split(';');
       for (var i = 0; i < cookies.length; i++) {
           var cookie = jQuery.trim(cookies[i]);
           // Does this cookie string begin with the name we want?
           if (cookie.substring(0, name.length + 1) === (name + '=')) {
               cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
               break;
           }
       }
   }
   return cookieValue;
}
function csrfSafeMethod(method) {
   // these HTTP methods do not require CSRF protection
   return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function login()
{
    getToken();
    var form = document.getElementById("login_form")
    var is_valid = form.reportValidity();
    if (!is_valid)
    {
        return ;
    }
    $.ajax({
       type: "POST",
       url: "/webservice",
       headers:{
            'action': 'login',
       },
       data: {
           'email': document.getElementById('login_email').value,
           'password': document.getElementById('login_password').value
       },
       success: function(msg) {
           console.log(msg);
           if(msg.error_code == 0){
                window.location.href="/";
           }else{
                //pop up error
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">'+msg.error_msg+'</span>',
                })
            }
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 500){
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">Error Login </span>' + errorThrown,
                })
            }
       },timeout: 60000
    });
}

function logout()
{
    getToken();
    $.ajax({
       type: "POST",
       url: "/webservice",
       headers:{
            'action': 'logout',
       },
       data: {

       },
       success: function(msg) {
           console.log(msg);
           if(msg.error_code == 0){
                window.location.href="/login";
           }else{
                //pop up error
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">'+msg.error_msg+'</span>',
                })
            }
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 500){
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">Error Logout </span>' + errorThrown,
                })
            }
       },timeout: 60000
    });
}

function signup()
{
    data = {

    }
    var form_data = new FormData();
    var form = document.getElementById("signup_form")
    var is_valid = form.reportValidity();
    if (!is_valid)
    {
        return ;
    }
    if (document.getElementById('password').value != document.getElementById('confirm_password').value)
    {
        Swal.fire({
          type: 'error',
          title: 'Oops!',
          html: '<span style="color: black;">Password and Confirm Password does not match</span>',
        })
        return;
    }
    for ( var i = 0; i < form.elements.length; i++ ) {
       var e = form.elements[i];
       console.log(e);
       if(e.classList.contains('form-input-element'))
       {
           if(e.type == 'checkbox')
           {
                remove_test = e.name.split('remove_file_');
                if(remove_test.length > 1)
                {
                    if(e.checked)
                    {
                        console.log('lalala');
                        data[remove_test[1]] = 'file_removed_check_500';
                    }
                }
                else
                {
                    data[e.name] = e.checked;
                }
           }
           else if (e.type == 'file')
           {
                var file = e.files[0];
                form_data.append(e.name, file);
           }
           else if (e.type == 'number')
           {
                if(e.value != '')
                    data[e.name] = parseFloat(e.value);
                else
                    data[e.name] = parseFloat(0);
           }
           else
           {
                if(e.value)
                {
                    data[e.name] = e.value;
                }
                else
                {
                    data[e.name] = '';
                }
           }
       }
    }
    form_data.append('data', JSON.stringify(data));
    getToken();
    $.ajax({
       type: "POST",
       url: "/webservice",
       headers:{
            'action': 'signup',
       },
       data: form_data,
       contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
       processData: false, // NEEDED, DON'T OMIT THIS
       success: function(msg) {
           console.log(msg);
           if(msg.error_code == 0){
                window.location.href="/login";
           }else{
                //pop up error
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">'+msg.error_msg+'</span>',
                })
            }
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 500){
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">Error Signup </span>' + errorThrown,
                })
            }
       },timeout: 60000
    });
}

function clear_file(element_id)
{
    document.getElementById(element_id).value = '';
}

function add_record(table_name)
{
    data = {
        'table_name': table_name
    }
    var form_data = new FormData();
    var form = document.getElementById("data_form")
    var is_valid = form.reportValidity();
    if (!is_valid)
    {
        return ;
    }
    for ( var i = 0; i < form.elements.length; i++ ) {
       var e = form.elements[i];
       console.log(e);
       if(e.classList.contains('remove-file-check') && e.type == 'checkbox')
       {
            remove_test = e.name.split('remove_file_');
            if(remove_test.length > 1)
            {
                if(e.checked)
                {
                    data[remove_test[1]] = '';
                }
            }
       }

       if(e.classList.contains('form-input-element'))
       {
           if(e.type == 'checkbox')
           {
                data[e.name] = e.checked;
           }
           else if (e.type == 'file')
           {
                var file_removed = false;
                if (document.getElementById('remove_file_' + e.name))
                {
                    var file_removed = document.getElementById('remove_file_' + e.name).checked;
                }

                if (!file_removed)
                {
                    var file = e.files[0];
                    form_data.append(e.name, file);
                }
           }
           else if (e.type == 'number')
           {
                if(e.value && e.value != '' && e.value != 'False')
                    data[e.name] = parseFloat(e.value);
                else
                    data[e.name] = parseFloat(0);
           }
           else
           {
                if(e.value)
                {
                    data[e.name] = e.value;
                }
                else
                {
                    data[e.name] = '';
                }
           }
       }
    }
    form_data.append('data', JSON.stringify(data));
    getToken();
    $.ajax({
       type: "POST",
       url: "/webservice",
       headers:{
            'action': 'add_record',
       },
       data: form_data,
       contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
       processData: false, // NEEDED, DON'T OMIT THIS
       success: function(msg) {
           console.log(msg);
           if(msg.error_code == 0){
                window.location.href="/tables/" + table_name + "/" + msg.response.id;
           }else{
                //pop up error
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">'+msg.error_msg+'</span>',
                })
            }
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 500){
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">Error Add Records </span>' + errorThrown,
                })
            }
       },timeout: 60000
    });
}

function update_record(table_name, rec_id)
{
    data = {
        'table_name': table_name,
        'record_id': rec_id
    }
    var form_data = new FormData();
    var form = document.getElementById("data_form")
    var is_valid = form.reportValidity();
    if (!is_valid)
    {
        return ;
    }
    for ( var i = 0; i < form.elements.length; i++ ) {
       var e = form.elements[i];
       console.log(e);
       if(e.classList.contains('remove-file-check') && e.type == 'checkbox')
       {
            remove_test = e.name.split('remove_file_');
            if(remove_test.length > 1)
            {
                if(e.checked)
                {
                    data[remove_test[1]] = '';
                }
            }
       }

       if(e.classList.contains('form-input-element'))
       {
           if(e.type == 'checkbox')
           {
                data[e.name] = e.checked;
           }
           else if (e.type == 'file')
           {
                var file_removed = false;
                if (document.getElementById('remove_file_' + e.name))
                {
                    var file_removed = document.getElementById('remove_file_' + e.name).checked;
                }

                if (!file_removed)
                {
                    var file = e.files[0];
                    form_data.append(e.name, file);
                }
           }
           else if (e.type == 'number')
           {
                if(e.value && e.value != '' && e.value != 'False')
                    data[e.name] = parseFloat(e.value);
                else
                    data[e.name] = parseFloat(0);
           }
           else if (e.type == 'password')
           {
                if(e.value && e.value != '')
                {
                    data[e.name] = e.value;
                }
           }
           else
           {
                if(e.value)
                {
                    data[e.name] = e.value;
                }
                else
                {
                    data[e.name] = '';
                }
           }
       }
    }
    form_data.append('data', JSON.stringify(data));
    getToken();
    $.ajax({
       type: "POST",
       url: "/webservice",
       headers:{
            'action': 'update_record',
       },
       data: form_data,
       contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
       processData: false, // NEEDED, DON'T OMIT THIS
       success: function(msg) {
           console.log(msg);
           if(msg.error_code == 0){
                window.location.href="/tables/" + table_name + "/" + msg.response.id;
           }else{
                //pop up error
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">'+msg.error_msg+'</span>',
                })
            }
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 500){
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">Error Update Records </span>' + errorThrown,
                })
            }
       },timeout: 60000
    });
}

function delete_record(table_name, rec_id)
{
    data = {
        'table_name': table_name,
        'record_id_list': rec_id
    }
    var form_data = new FormData();
    form_data.append('data', JSON.stringify(data));
    getToken();
    $.ajax({
       type: "POST",
       url: "/webservice",
       headers:{
            'action': 'delete_multi_records',
       },
       data: form_data,
       contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
       processData: false, // NEEDED, DON'T OMIT THIS
       success: function(msg) {
           console.log(msg);
           if(msg.error_code == 0){
                window.location.href="/tables/" + table_name;
           }else{
                //pop up error
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">'+msg.error_msg+'</span>',
                })
            }
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 500){
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">Error Delete Records </span>' + errorThrown,
                })
            }
       },timeout: 60000
    });
}

function delete_confirmation(table_name, rec_id='', data_count=0)
{
    console.log(rec_id);
    if (rec_id == '')
    {
        if (data_count != 0)
        {
            list_id = [];
            for (i = 0; i < data_count; i++) {
                if(document.getElementById('checkbox_'+table_name.toString()+'_'+(i+1).toString()).checked)
                {
                    list_id.push(document.getElementById('hidden_'+table_name.toString()+'_'+(i+1).toString()).value);
                }
            }
            console.log(list_id);
            if (list_id.length != 0)
            {
                console.log(list_id);
                rec_id = list_id;
                Swal.fire({
                  title: 'Are you sure you want to delete these records? (' + list_id.length + ' records selected)',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes'
                }).then((result) => {
                  if (result.value) {
                     delete_record(table_name, JSON.stringify(rec_id));
                  }
                })
            }
            else
            {
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">There is no selected record! </span>',
                })
            }
        }
        else
        {
            Swal.fire({
              type: 'error',
              title: 'Oops!',
              html: '<span style="color: red;">There is no selected record! </span>',
            })
        }
    }
    else
    {
        Swal.fire({
          title: 'Are you sure you want to delete this record?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.value) {
             delete_record(table_name, JSON.stringify([rec_id]));
          }
        })
    }
}

function duplicate_record(table_name, rec_id)
{
    data = {
        'table_name': table_name,
        'record_id': rec_id
    }
    var form_data = new FormData();
    form_data.append('data', JSON.stringify(data));
    getToken();
    $.ajax({
       type: "POST",
       url: "/webservice",
       headers:{
            'action': 'duplicate_record',
       },
       data: form_data,
       contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
       processData: false, // NEEDED, DON'T OMIT THIS
       success: function(msg) {
           console.log(msg);
           if(msg.error_code == 0){
                window.location.href="/tables/" + table_name + "/" + msg.response.id;
           }else{
                //pop up error
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">'+msg.error_msg+'</span>',
                })
            }
       },
       error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 500){
                Swal.fire({
                  type: 'error',
                  title: 'Oops!',
                  html: '<span style="color: red;">Error Duplicate Records </span>' + errorThrown,
                })
            }
       },timeout: 60000
    });
}

function go_to_page_more(page_params, page_start, page_stop, count_page){
    var page_number_start = parseInt(page_start);
    var page_number_stop = parseInt(page_stop);
    page_from_params = document.getElementById("gotopageID").value;
    document.getElementById('pagination').value = page_from_params;

    if(page_from_params >= page_number_start && page_from_params <= page_number_stop){
        document.getElementById('search_filter').submit();
    }else{
        alert("The page you are looking for does not exist.")
    }
}