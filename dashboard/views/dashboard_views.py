from django.shortcuts import render, redirect
from ..models import *
import sys
from django.contrib.staticfiles.templatetags.staticfiles import static
import types
from webservice.urls.webservice_urls import *
import json
from django.db.models import Q
from django.conf import settings
import locale
import logging, traceback
import copy
from tools.db_tables import db_tables
from alumni_gloria_admin import firebase

locale.setlocale(locale.LC_TIME, "en_US.UTF-8")
_logger = logging.getLogger("gloria_admin_logger")


def login(request):
    if request.session.get('logged_in_user'):
        return redirect('dashboard:index')

    context = {

    }
    return render(request, 'login.html', context)


def signup(request):
    if request.session.get('logged_in_user'):
        return redirect('dashboard:index')

    context = {

    }
    return render(request, 'signup.html', context)


def index(request):
    if not request.session.get('logged_in_user'):
        return redirect('dashboard:login')

    context = {
        'user': request.session['logged_in_user'],
        'table_name': '',
        'tables': db_tables,
        'mode': 'index'
    }
    return render(request, 'index.html', context)


def tree_view(request, table_name):
    if not request.session.get('logged_in_user'):
        return redirect('dashboard:login')

    data = {}
    for rec in db_tables:
        if rec['name'] == table_name:
            data = rec.copy()

    if not data:
        return render(request, '404.html', {})

    data.update({
        'records': []
    })
    if request.POST.get('search_params'):
        # search_list_fmt = [
        #     {
        #         'key': 'record_key',
        #         'operator': '=',
        #         'value': 'record_value'
        #     }
        # ]

        search_params_list = json.loads(request.POST['search_params'])
        search_params = []
        for rec in search_params_list:
            search_params.append((rec['key'], rec['operator'], rec['value']))
        data_records = firebase.search_data(table_name, search_params)
    else:
        data_records = firebase.get_data(table_name)
    if not data_records.get('error_code'):
        data['records'] = data_records['response']
        for rec in data['records']:
            for dat_key in data['fields']:
                if not rec.get(dat_key['name']):
                    rec.update({
                        dat_key['name']: False
                    })
    else:
        _logger.info('Failed to get tree view data from database: %s' % data_records['error_msg'])

    context = {
        'user': request.session['logged_in_user'],
        'table_name': table_name,
        'table_display': data['display'],
        'tables': db_tables,
        'mode': 'tree_view',
        'data': data,
        'data_count': data.get('records') and len(data['records']) or 0
    }
    return render(request, 'index.html', context)


def form_view(request, table_name, record_id='', mode='form_view'):
    if not request.session.get('logged_in_user'):
        return redirect('dashboard:login')

    data = {}
    for rec in db_tables:
        if rec['name'] == table_name:
            data = rec.copy()

    if not data:
        return render(request, '404.html', {})

    if mode != 'add':
        data_records = firebase.get_data(table_name, record_id)
        if not data_records.get('error_code'):
            for rec in data['fields']:
                if rec['type'] == 'one2many':
                    one2many_records = firebase.search_data(rec['related_table'], [(rec['related_field'], '==', record_id, table_name)], is_new=True)
                    if not one2many_records.get('error_code'):
                        for temp_pop in one2many_records['response']:
                            temp_pop.pop(rec['related_field'])
                        rec.update({
                            'value': one2many_records['response']
                        })
                    else:
                        rec.update({
                            'value': []
                        })
                        _logger.info('Failed to get one2many data for table % from database: %s' % (rec['related_table'], one2many_records['error_msg']))
                else:
                    rec.update({
                        'value': data_records['response'].get(rec['name']) and data_records['response'][rec['name']] or ''
                    })
        else:
            _logger.info('Failed to get form view data from database: %s' % data_records['error_msg'])

    for rec in data['fields']:
        if rec['type'] == 'many2one':
            many2one_records = firebase.get_data(rec['related_table'])
            if not many2one_records.get('error_code'):
                final_list = []
                for many2one_rec in many2one_records['response']:
                    if rec['related_table'] in ['users']:
                        many2one_tuple = (many2one_rec['id'], many2one_rec.get('first_name', '') + ' ' + many2one_rec.get('last_name', ''))
                    else:
                        many2one_tuple = (many2one_rec['id'], many2one_rec.get('name', many2one_rec['id']))
                    final_list.append(many2one_tuple)
                rec.update({
                    'options': final_list
                })
            else:
                _logger.info('Failed to get many2one data for table % from database: %s' % (rec['related_table'], many2one_records['error_msg']))

    context = {
        'user': request.session['logged_in_user'],
        'table_name': table_name,
        'table_display': data['display'],
        'record_id': record_id,
        'tables': db_tables,
        'mode': mode,
        'data': data
    }
    return render(request, 'index.html', context)
